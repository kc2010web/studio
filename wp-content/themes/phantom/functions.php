<?php

function dz($var, $die = false)
{
    
        $trace = debug_backtrace();
        echo '<div style="display: block; position: relative; background-color: #ffffff; border: 1px solid #000000; padding: 5px; margin: 0;">';
        echo '<div style="display: block; position: relative; color: #808080; font-size: 12px; margin: 0; padding: 0;">from (<b>'.$trace[0]['file'].'</b>) on line <b>'.$trace[0]['line'].'</b></div><pre>';
        if(is_array($var)) print_r($var); else var_dump($var);
        echo '</pre></div>';

        if($die)
        {
            die();
        }
    
}

if ( ! function_exists( 'phantom_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 *
 * Create your own twentysixteen_setup() function to override in a child theme.
 *
 * @since Twenty Sixteen 1.0
 */
function phantom_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentysixteen
	 * If you're building a theme based on Twenty Sixteen, use a find and replace
	 * to change 'twentysixteen' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'phantom' );

	// Add default posts and comments RSS feed links to head.
	//add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for custom logo.
	 *
	 *  @since Twenty Sixteen 1.2
	 */
	add_theme_support( 'custom-logo', array(
		'height'      => 43,
		'width'       => 43,
		//'flex-height' => true,
                'flex-width' => true,
	) );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 1200, 9999 );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'phantom' ),
		'social'  => __( 'Social Links Menu', 'phantom' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'status',
		'audio',
		'chat',
	) );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, icons, and column width.
	 */
	//add_editor_style( array( 'css/editor-style.css', twentysixteen_fonts_url() ) );

	// Indicate widget sidebars can use selective refresh in the Customizer.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif; // twentysixteen_setup
add_action( 'after_setup_theme', 'phantom_setup' );

function custom_mtypes( $m ){
    //$m['svg'] = 'image/svg+xml';
    //$m['svgz'] = 'image/svg+xml';
    $m['svg'] = 'application/scalable_vector_graphic';
    $m['svgz'] = 'application/scalable_vector_graphic';
    return $m;
}
add_filter( 'upload_mimes', 'custom_mtypes' );

function phantom_scripts() {
	
	// Load the html5 shiv.
	wp_enqueue_script( 'phantom-html5', get_template_directory_uri() . '/assets/js/ie/html5.js', array(), '3.6.2' );
	wp_script_add_data( 'phantom-html5', 'conditional', 'lt IE 8' );
        
        wp_enqueue_style( 'phantom-main', get_template_directory_uri() . '/assets/css/main.css');
        
	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'phantom-ie9', get_template_directory_uri() . '/assets/css/ie9.css', array( 'phantom-main' ), '20160919' );
	wp_style_add_data( 'phantom-ie9', 'conditional', 'lt IE 9' );
        
        // Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'phantom-ie8', get_template_directory_uri() . '/assets/css/ie8.css', array( 'phantom-main' ), '20160919' );
	wp_style_add_data( 'phantom-ie8', 'conditional', 'lt IE 8' );
        
}
add_action( 'wp_enqueue_scripts', 'phantom_scripts' );

function phantom_footer_scripts() {
	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), null, true );
        wp_enqueue_script( 'jquery');
        
        wp_register_script( 'phantom', get_template_directory_uri() . '/assets/js/skel.min.js', array('jquery'), null, true );
        wp_enqueue_script( 'phantom');
         
        wp_register_script( 'phantom-util', get_template_directory_uri() . '/assets/js/util.js', array('jquery'), null, true );
        wp_enqueue_script( 'phantom-util');
         
        wp_register_script( 'phantom-ie', get_template_directory_uri() . '/assets/js/ie/respond.min.js', array('jquery'), null, true );
        wp_enqueue_script( 'phantom-ie');
        wp_script_add_data( 'phantom-ie', 'conditional', 'lt IE 8' );
        
        wp_register_script( 'phantom-main', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), null, true );
        wp_enqueue_script( 'phantom-main');
}
 
add_action( 'wp_enqueue_scripts', 'phantom_footer_scripts' );

function phantom_body_classes( $classes ) {
	return array();
}
add_filter( 'body_class', 'phantom_body_classes' );

add_filter( 'get_custom_logo', 'phantom_logo' );

function phantom_logo() {
    
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $html = sprintf( '<a href="%1$s" class="logo"><span class="symbol">%2$s</span><span class="title">%3$s</span></a>',
            esc_url( '/' ),
            wp_get_attachment_image( $custom_logo_id, 'full', false, array(
                'class'    => '',
            )),
            'Web-studio'
        );
    
    return $html;   
}

add_filter('nav_menu_css_class', 'clear_nav_menu_item_class', 10, 3); 
function clear_nav_menu_item_class($classes, $item, $args) { 
    return array(); 
}
//add_image_size( 'main_page', 412, 381, true );