<!DOCTYPE html>
<!--
	Phantom by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- Wrapper -->
    <div id="wrapper">
	<!-- Header -->
	<header id="header">
            <div class="inner">
                <!-- Logo -->
                <?php the_custom_logo(); ?>
               
                <!-- Nav -->
                <?php if ( has_nav_menu( 'primary' ) ) : ?>
		    <nav aria-label="<?php esc_attr_e( 'Primary Menu', 'phantom' ); ?>">
                        <ul>
                            <li><a href="#menu">Menu</a></li>
                        </ul>
		    </nav><!-- .main-navigation -->
		<?php endif; ?>
               

            </div>
        </header>

        <!-- Menu -->
        <nav id="menu">
                <h2>Меню</h2>
                <?php if ( has_nav_menu( 'primary' ) ) : ?>
                        <?php
                        
                                wp_nav_menu( array(
                                        'menu' => 'Menu',
                                        'container'         => '',
                                        'menu_class'        => '', 
                                        'menu_id'           => '',
                                 ) );
                        ?>
                <?php endif; ?>
             
        </nav>
        
	<!-- Main -->
	<div id="main">
            <div class="inner">
