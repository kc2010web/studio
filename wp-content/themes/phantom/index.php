<?php
get_header(); ?>
<header>
    <h1>This is imba site of imba web-studio</h1>
</header>
<section class="tiles">

<?php
$posts = get_posts( array(
	'numberposts'     => -1, // ���� ����� ��� posts_per_page
	'offset'          => 0,
	'orderby'         => 'post_date',
	'order'           => 'DESC',
	'post_type'       => 'projects',
	'post_status'     => 'publish'
        //'post_status'     => 'any'
) );
//dz($posts);

 if ( count($posts) > 0) {
 
        foreach ($posts as $post){
            setup_postdata( $post );
 ?>
   

        <article class="style<?=rand(1, 6)?>">
                <span class="image">
                    <?php if (strlen(get('screenshots')) > 0): ?>
                        <?=get_image('screenshots')?>
                    <?php else: ?>
                        <img src="<?=get_template_directory_uri()?>/images/pic0<?=rand(1,9)?>.jpg" alt="" />
                    <?php endif; ?>
                </span>
                <a href="<?=get_permalink()?>" rel="nofollow">
                        <h2><?=get('name')?></h2>
                        <div class="content">
                                <p><?=get('preview_text')?></p>
                        </div>
                </a>
        </article>
        

<?php
        }
 } ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
